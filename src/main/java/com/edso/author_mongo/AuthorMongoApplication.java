package com.edso.author_mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthorMongoApplication.class, args);
	}

}
