package com.edso.author_mongo.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
public class AuthorCommand {
    private String id;

    @NotBlank
    private String name;
}
