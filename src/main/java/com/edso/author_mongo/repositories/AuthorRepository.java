package com.edso.author_mongo.repositories;

import com.edso.author_mongo.domains.Author;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorRepository extends MongoRepository<Author, String> {
    @Override
    Optional<Author> findById(String id);

    @Override
    void deleteById(String s);
}
