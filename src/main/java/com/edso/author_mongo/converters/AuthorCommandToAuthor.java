package com.edso.author_mongo.converters;

import com.edso.author_mongo.commands.AuthorCommand;
import com.edso.author_mongo.domains.Author;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class AuthorCommandToAuthor implements Converter<AuthorCommand, Author> {

    @Synchronized
    @Nullable
    @Override
    public Author convert(AuthorCommand source) {
        if(source == null) return null;

        final Author author = new Author();
        author.setId(source.getId());
        author.setName(source.getName());
        return author;
    }
}