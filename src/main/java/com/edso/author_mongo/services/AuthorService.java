package com.edso.author_mongo.services;

import com.edso.author_mongo.commands.AuthorCommand;
import com.edso.author_mongo.domains.Author;

import java.util.Optional;
import java.util.Set;

public interface AuthorService {
    Set<Author> getAuthors();
    void updateAuthorName(String id, String name);
    //void updateAuthorWithRollbackCustom(String id, String name);
    AuthorCommand saveAuthorCommand(AuthorCommand command);
    void deleteById(String id);
    Optional<Author> findById(String id);
}
