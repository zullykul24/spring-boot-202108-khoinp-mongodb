package com.edso.author_mongo.services;

import com.edso.author_mongo.commands.AuthorCommand;
import com.edso.author_mongo.converters.AuthorCommandToAuthor;
import com.edso.author_mongo.converters.AuthorToAuthorCommand;
import com.edso.author_mongo.domains.Author;
import com.edso.author_mongo.exceptions.NotFoundException;
import com.edso.author_mongo.repositories.AuthorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    private final AuthorCommandToAuthor authorCommandToAuthor;
    private final AuthorToAuthorCommand authorToAuthorCommand;

    public AuthorServiceImpl(AuthorRepository authorRepository, AuthorCommandToAuthor authorCommandToAuthor,
                             AuthorToAuthorCommand authorToAuthorCommand) {
        this.authorRepository = authorRepository;
        this.authorCommandToAuthor = authorCommandToAuthor;
        this.authorToAuthorCommand = authorToAuthorCommand;
    }

    @Override
    public Set<Author> getAuthors() {
        Set<Author> authors = new HashSet<>();
        authorRepository.findAll().iterator().forEachRemaining(authors::add);
        return authors;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateAuthorName(String id, String name) {
        Author author = authorRepository.findById(id).get();
        author.setName(name);
    }

//    @Override
//    @Transactional(rollbackFor = Exception.class, noRollbackFor = EntityNotFoundException.class)
//    public void updateAuthorWithRollbackCustom(String id, String name) {
//        Author author = authorRepository.findById(id).orElse(null);
//        author.setName(name);
//    }

    @Override
    @Transactional
    public AuthorCommand saveAuthorCommand(AuthorCommand command) {
        Author detachedAuthor = authorCommandToAuthor.convert(command);
        Author savedAuthor = authorRepository.save(detachedAuthor);
        log.debug("saved author id: " + savedAuthor.getId());
        return authorToAuthorCommand.convert(savedAuthor);
    }

    @Override
    public void deleteById(String id) {
        authorRepository.deleteById(id);
    }

    @Override
    public Optional<Author> findById(String id) {
        Optional<Author> authorOptional =  authorRepository.findById(id);
        if(!authorOptional.isPresent()){
            throw new NotFoundException("Author id not found, with id = " + id);
        }

        return authorOptional;
    }
}
