package com.edso.author_mongo.controllers;

import com.edso.author_mongo.commands.AuthorCommand;
import com.edso.author_mongo.services.AuthorService;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

class IndexControllerTest {

    @Mock
    AuthorService authorService;

    IndexController indexController;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        indexController = new IndexController(authorService);
        mockMvc = MockMvcBuilders.standaloneSetup(indexController)
                .setControllerAdvice(new ControllerExceptionHandler())
                .build();
    }

    @Test
    public void testPostNewRecipeForm() throws Exception {
        AuthorCommand command = new AuthorCommand();
        command.setId("");

        when(authorService.saveAuthorCommand(any())).thenReturn(command);

        mockMvc.perform(post("/author")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("id", "")
                        .param("name", "some string")
                )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }
}